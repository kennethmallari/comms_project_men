var express = require("express");
var router = express.Router();

var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/";

var uploadsList = "";

MongoClient.connect(url, function (err, db) {
  if (err) throw err;
  var dbo = db.db("communication");
  dbo
    .collection("uploads")
    .find({})
    .toArray(function (err, result) {
      if (err) throw err;
      uploadsList = result;
      console.log(result);
    });
});

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send(uploadsList);
});

module.exports = router;

//My old code:

// var express = require("express");
// var router = express.Router();

// const uploads = [
//   {
//     id: 1,
//     label: "Sales Report",
//     fileName: "sales-Sep2014.xls",
//   },
//   {
//     id: 2,
//     label: "Quarterly Summary",
//     fileName: "SummaryQ4-2014.ppt",
//   },
//   {
//     id: 3,
//     label: "Projection 2013-14",
//     fileName: "SalesProfitProjection.xls",
//   },
// ];

// router.get("/", function (req, res, next) {
//   res.send({ uploads });
// });

// module.exports = router;
